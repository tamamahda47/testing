const usersController = require("./controllers/users")();
const profileController = require("./controllers/profile")();

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

require("dotenv").config();

const port = process.env.PORT || 5500;

// Koneksi ke server mongo db
mongoose.connect(process.env.DATABASE, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

// Inisiasi app
const app = express();
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(usersController);
app.use(profileController);

app.listen(port, () => console.log(`this app run on port ${port}`));
