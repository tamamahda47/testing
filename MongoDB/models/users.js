const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = function UsersModel() {
  const UsersSchema = new Schema({
    profilesId: { type: mongoose.Schema.Types.ObjectId, ref: "profiles" },
    username: { type: String },
    password: { type: String },
    salt: { type: String },
  });

  return mongoose.model("users", UsersSchema);
};
