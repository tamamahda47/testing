const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = function profileModel() {
  const ProfilesSchema = new Schema({
    usersId: { type: mongoose.Schema.Types.ObjectId, ref: "users" },
    fulname: { type: String },
    email: { type: String },
    jk: { type: String },
    umur: { type: Number },
    alamat: { type: String },
  });

  return mongoose.model("profiles", ProfilesSchema);
};
