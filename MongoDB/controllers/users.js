const UsersModel = require("../models/users")();
const app = require("express").Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

const middlewareAuth = require("../middlewares/tokenauth");

module.exports = function usersController() {
  // Register users
  app.post("/register", async (req, res) => {
    try {
      const profilesId = req.body.profilesId;
      const username = req.body.username;
      const password = req.body.password;

      const saltkey = await bcrypt.genSalt(10);

      const hashPassword = await bcrypt.hash(password, saltkey);

      const data = await UsersModel.create({
        profilesId: profilesId,
        username: username,
        password: hashPassword,
        salt: saltkey,
      });

      res.json({
        message: "success insert new user",
        data: data,
      });
    } catch (error) {
      res.send(error.message);
    }
  });

  // Login Users
  app.post("/login", async (req, res) => {
    let statusCode = 500;

    try {
      const username = req.body.username;
      const password = req.body.password;

      // Cari data di MongoDB dengan username dari req.body
      const users = await UsersModel.findOne({ username: username });

      if (!users) {
        statusCode = 404;
        throw new Error("users not found");
      }

      // Membandingkan password dari req.body dengan password yang tersimpan di database
      const isPasswordMatch = await bcrypt.compare(password, users.password);
      if (!isPasswordMatch) {
        statusCode = 400;
        throw new Error("password invalid");
      }

      // generate token
      const token = jwt.sign(
        { username: users.username },
        process.env.SECRET_KEY
      );

      res.json({ message: "success login", data: { token: token } });
    } catch (error) {
      res.status(statusCode).json({ message: error.message });
    }
  });

  // get all users
  app.get("/users", middlewareAuth, async (req, res) => {
    try {
      const data = await UsersModel.find();

      res.json({ message: "sucess get data users", data: data });
    } catch (error) {
      res.send(error.message);
    }
  });

  // get users populate
  app.get("/users/id", middlewareAuth, async (req, res) => {
    try {
      const id = req.query.id;
      await UsersModel.find()
        .populate({
          path: "profilesId",
          select: "fulname email jk umur alamat",
        })
        .exec()
        .then((data) => {
          res.json({
            message: "succes get data users with profile",
            data: data,
          });
        });
    } catch (error) {
      res.send(error.message);
    }
  });

  // update users
  app.put("/users", middlewareAuth, async (req, res) => {
    await UsersModel.updateOne(
      { _id: req.body.id },
      { username: req.body.username, profilesId: req.body.profilesId }
    );
    res.json({ message: "sucess update" });
  });

  // delete users
  app.delete("/users", middlewareAuth, async (req, res) => {
    await UsersModel.deleteOne({ _id: req.query.id });

    res.json({ message: "sucess delete" });
  });

  return app;
};
