const ProfileModel = require("../models/profile")();
const app = require("express").Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

const middlewareAuth = require("../middlewares/tokenauth");

module.exports = function ProfileController() {
  // get all profile
  app.get("/profile", middlewareAuth, async (req, res) => {
    try {
      const data = await ProfileModel.find();
      res.json({ message: "succes get profile", data: data });
    } catch (error) {
      res.send(error.message);
    }
  });

  // Create new profile
  app.post("/profile", middlewareAuth, async (req, res) => {
    try {
      const data = await ProfileModel.create(req.body);
      res.json({ message: "succes create new profile", data: data });
    } catch (error) {
      res.send(error.message);
    }
  });

  // update profile
  app.put("/profile", middlewareAuth, async (req, res) => {
    await ProfileModel.updateOne(
      { _id: req.body.id },
      {
        fullname: req.body.fullname,
        email: req.body.email,
        jk: req.body.jk,
        umur: req.body.umur,
        alamat: req.body.alamat,
      }
    );
    res.json({ message: "sucess update" });
  });

  // delete profile
  app.delete("/profile", middlewareAuth, async (req, res) => {
    await ProfileModel.deleteOne({ _id: req.query.id });

    res.json({ message: "sucess delete profile" });
  });

  return app;
};
