const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const usersController = require("./controllers/users")();
const profileController = require("./controllers/profiles")();
require("dotenv").config();

const port = process.env.PORT || 6000;

const app = express();
app.use(express.json());

app.use(usersController);
app.use(profileController);

app.listen(port, () => console.log(`this app running on port ${port}`));
