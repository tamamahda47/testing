const jwt = require("jsonwebtoken");
require("dotenv").config();

// Middleware Auth
module.exports = function middlewareAuth(req, res, next) {
  try {
    // Split bearer token
    const token = req.headers.authorization.split(" ")[1];

    jwt.verify(token, process.env.SECRET_KEY);

    next();
  } catch (error) {
    res.status(400).json({ message: "token invalid" });
  }
};
