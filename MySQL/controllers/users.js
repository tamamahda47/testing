const db = require("../models");
const app = require("express").Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

const middlewareAuth = require("../middlewares/tokenAuth");

module.exports = function usersController() {
  // Endpoint Register
  app.post("/register", async (req, res) => {
    try {
      const username = req.body.username;
      const password = req.body.password;
      const saltkey = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(password, saltkey);

      const data = await db.users.create({
        username: username,
        password: hashPassword,
        salt: saltkey,
      });

      res.json({ message: "sucess create new user", data: data });
    } catch (error) {
      res.json({ message: error.message });
    }
  });

  // Endpoint login
  app.post("/login", async (req, res) => {
    try {
      const username = req.body.username;
      const password = req.body.password;

      const users = await db.users.findOne({ where: { username: username } });

      if (!users) {
        status = 404;
        throw new Error("users not found");
      }

      const isPasswordMatch = await bcrypt.compare(password, users.password);
      if (!isPasswordMatch) {
        statusCode = 400;
        throw new Error("password invalid");
      }

      const token = jwt.sign({ username: users.username }, "t4m4m4hd4");

      res.json({ message: "succes login", data: { token: token } });
    } catch (error) {
      res.json({ message: error.message });
    }
  });

  // Get all users
  app.get("/users", middlewareAuth, async (req, res) => {
    const data = await db.users.findAll();
    res.json({ message: "succes get users", data: data });
  });

  // Update user
  app.put("/users", middlewareAuth, async (req, res) => {
    await db.users.update(
      {
        username: req.body.username,
      },
      { where: { id: req.body.id } }
    );
    res.json({ message: "sucess update" });
  });

  // delete users
  app.delete("/users", middlewareAuth, async (req, res) => {
    const id = req.query.id;
    await db.users.destroy({ where: { id: id } });

    res.json({ message: "sucess delete", id: id });
  });

  return app;
};
