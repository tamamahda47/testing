const db = require("../models");
const app = require("express").Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

const middlewareAuth = require("../middlewares/tokenAuth");

module.exports = function ProfileController() {
  // get all profile
  app.get("/profile", middlewareAuth, async (req, res) => {
    try {
      const data = await db.profiles.findAll();
      res.json({ message: "succes get profile", data: data });
    } catch (error) {
      res.send(error.message);
    }
  });

  // Create new profile
  app.post("/profile", middlewareAuth, async (req, res) => {
    try {
      const data = await db.profiles.create(req.body);
      res.json({ message: "succes create new profile", data: data });
    } catch (error) {
      res.send(error.message);
    }
  });

  // update users
  app.put("/profile", middlewareAuth, async (req, res) => {
    try {
      await db.profiles.update(
        {
          fullname: req.body.fullname,
          email: req.body.email,
          jk: req.body.jk,
          umur: req.body.umur,
          alamat: req.body.alamat,
        },
        { where: { id: req.body.id } }
      );
      res.json({ message: "sucess update" });
    } catch (error) {
      res.send(error.message);
    }
  });

  // delete users
  app.delete("/profile", middlewareAuth, async (req, res) => {
    try {
      await db.profiles.destroy({ where: { id: req.query.id } });

      res.json({ message: "sucess delete profile" });
    } catch (error) {
      res.send(error.message);
    }
  });

  return app;
};
