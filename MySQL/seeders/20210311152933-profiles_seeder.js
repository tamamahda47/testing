"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     */
    await queryInterface.bulkInsert(
      "profiles",
      [
        {
          usersId: 4,
          fullname: "tamam ahda septiadi",
          email: "tamamahda47@gmail.com",
          jk: "laki-laki",
          umur: 23,
          alamat: "Bogor dong",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          usersId: 3,
          fullname: "muhammad munir",
          email: "munir@gmail.com",
          jk: "laki-laki",
          umur: 30,
          alamat: "Makassar",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     */
    await queryInterface.bulkDelete("profiles", null, {});
  },
};
