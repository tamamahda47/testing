"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     */
    await queryInterface.bulkInsert(
      "users",
      [
        {
          username: "John Doe",
          password: "john12345",
          salt: "blablabla",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "Tamam Ahda Septiadi",
          password: "tamam12345",
          salt: "blablablajuga",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     */
    await queryInterface.bulkDelete("users", null, {});
  },
};
